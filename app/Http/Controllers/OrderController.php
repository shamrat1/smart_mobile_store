<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Notifications\OrderAdded;
use App\Notifications\PurchaseInvoice;
use App\Order;
use App\OrderItems;
use App\Product;
use App\Subproduct;
use App\User;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class OrderController extends Controller
{
    public function order(){
        var_dump(Cart::content());
    }
    public function index(){
        $orders = Order::all();
        return view('order.index')->withOrders($orders);
    }

    public function store(Request $request){
        //dd($request->all());

        //order Table
        $order = new Order;
        $this->validate($request,[
            'name'=>'required|max:30',
            'mobile'=>'required|max:15',
            'address'=>'required|max:190',
            'city'=>'required|max:30'
        ]);


        try{
            $order->user_id = Auth::user()->id;
            $order->name = $request->name;
            $order->mobile = $request->mobile;
            $order->address = $request->address;
            $order->city = $request->city;
            $order->payment_type = "credit-card";
            $order->approved = 0;

            $order->save();

            //end of order table operation

            //start of Order Items Table Operation

            for ($i=1;$i<=Cart::count();$i++){
                foreach (Cart::content() as $item){
                    $orderItems = new OrderItems;
                    $orderItems->product_name = $item->name;
                    $orderItems->product_id = $item->id;
                    $orderItems->price = $item->price;
                    $orderItems->type = $item->options->type;
                    $orderItems->quantity = $item->qty;
                    $orderItems->order_id = $order->id;
                    $orderItems->save();
                }
            }

            //Charging Customer

            Stripe::setApiKey(env('STRIPE_SECRET'));

            $customer = Customer::create(array(
                'email' => Auth::user()->email,
                'source' => $request->stripeToken
            ));


            $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount' => round(Cart::total())*100,
                'currency' => 'usd'
            ));
            //end of charging customer

            Cart::destroy();

            $admin = Admin::find(1);
            $admin->notify(new OrderAdded($order));
            Session::flash('message','Order is added successfully. You will be notified after verification.');
            return redirect()->route('index');

        }catch (\Exception $exception){
            return $exception->getMessage();
        }



    }

    public function orderDetails(Request $request){

    }

    public function isApproved($id){

        try{
            $order = Order::find($id);

            $order->approved = 1;
            $order->save();
//
            $items = OrderItems::where('order_id','=',$id)->get();
//        dd($items->all());
            foreach ($items as $item){
//            dd($item->product_name);

                if ($item->type == "product"){
                    $product = Product::find($item->product_id);
                    $qty = $product->quantity;
                    $product->quantity = $qty - $item->quantity;
                    $product->save();
                }
                else{
                    $subproduct = Subproduct::find($item->product_id);
                    $qty = $subproduct->quantity;
                    $subproduct->quantity = $qty - $item->quantity;
                    $subproduct->save();
                }
            }
            $user = User::find($order->user_id);
            $user->notify(new PurchaseInvoice($order,$items));
            Session::flash('message','Order is approved.');
            return redirect()->back();
        }catch (\Exception $exception){
            return $exception->getMessage();
        }

    }

    public function allOrderItems(){
        $items = OrderItems::all();

        return view('admin.orderItems')->withItems($items);
    }

}
