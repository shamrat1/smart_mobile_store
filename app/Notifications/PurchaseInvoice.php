<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PurchaseInvoice extends Notification
{
    use Queueable;
    private $name;
    private $address;
    private $city;
    private $items;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($orderDetails,$Orderitems)
    {
        $this->name = $orderDetails->name;
        $this->city = $orderDetails->city;
        $this->address = $orderDetails->address;
        $this->items = $Orderitems;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting("Hello $this->name,")
                    ->line('Your order is approved and shipped in the following address')
                    ->line("Address: $this->address, $this->city")
                    ->from('noreplay@smstore.com')
                    ->line('Thank you for shopping with us!');

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            $this->items
        ];
    }
}
