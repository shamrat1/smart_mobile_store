@extends('searchLayout')

@section('title','Your Search Results')

@section('search','Product Search')

@section('content')

            <div class="container">
                <h3> The search results for your query "{{ $query }}" is :</h3>

                @if($products->count() > 0)

                    <table class="table well">
                        <thead>
                        <tr>
                            <th>Mobile</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $item)
                            <tr>
                                <td><a href="{{ route('pages.single',$item->slug) }}">{{$item->name}}</a></td>
                                <td>{{$item->description}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif



                @if($accessories->count() > 0)

                    <table class="table well">
                        <thead>
                        <tr>
                            <th>Accessory</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($accessories as $item)
                            <tr>
                                <td><a href="{{ route('pages.single',$item->slug) }}">{{$item->name}}</a></td>
                                <td>{{$item->description}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

                @if($accessories->count() == 0 & $products->count() == 0)

                    <div class="well col-lg-push-6" style="margin-bottom: 300px;margin-top: 20px">
                        {{ $message }}
                    </div>



                    @endif

            </div>

    @endsection