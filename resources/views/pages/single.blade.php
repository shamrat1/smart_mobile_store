
@extends('alt')
@section('title',"$product->name")
@section('css')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
@endsection

@section('content')

    <div class="w3l_banner_nav_right">
        <div class="w3l_banner_nav_right_banner3">
            <h3>Best Deals For New Products<span class="blink_me"></span></h3>
        </div>
        <div class="agileinfo_single">
            <h5>{{ $product->name }}</h5>
            <small>Rated</small>&nbsp;<h2>@if($average != null)
                                    {{ number_format($average,2) }}
                                    @else
                                    Not Rated Yet.
                                    @endif
            </h2>

            <div class="col-md-4 agileinfo_single_left">
                <img id="example" src="{{ url('images/'.$product->images)}}" alt=" " class="img-responsive" />
            </div>

            <div class="col-md-8 agileinfo_single_right">
                <div class="row">
                <div class="rating1" style="margin-bottom: 10px">
						<span class="starRating">
							<input id="rating5" type="radio" name="rating" value="5">
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4">
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3" checked>
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2">
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1">
							<label for="rating1">1</label>
						</span>
                </div>

                        &nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('review.create',$product->slug) }}" class="btn btn-default">Add review to this product</a>
                </div>
                <div class="w3agile_description">
                    <h4>Description :</h4>
                    <p>{{ $product->description }}</p>
                </div>
                <div class="snipcart-item block">
                    <div class="snipcart-thumb agileinfo_single_right_snipcart">
                        <h4>${{ $product->unit_price }}<span>${{ $product->discount }}</span></h4>
                    </div>
                    <div class="snipcart-details agileinfo_single_right_details">
                        <a href="{{ route('cart.create',$product->slug) }}" class="btn btn-primary">Add To Cart</a>
                    </div>
                </div>
            </div>

            <div class="clearfix"> </div>
            <hr>
            <div class="col-md-9">
                @if(!empty($reviews))
                    <h2> Some Reviews of {{ $product->name }}</h2><br>
                @else
                    <div class="row">
                    <h2>No Reviews are posted yet.</h2> &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{ route('review.create',$product->slug) }}" class="btn btn-primary">Add review</a>
                    </div>
                @endif
                @php
                $increment = 0
                @endphp
                @foreach($reviews as $review)

                    <div class="card border-primary mb-3" style="max-width: 20rem;">
                        <div class="card-header">Rating : {{ $review->rating }}</div>
                        <div class="card-body">

                            <h4 class="card-title">{{ $review->user->name }}</h4>

                            <p class="card-text">{{ $review->review }}</p>
                        </div>
                    </div>
                    <div class="comment well col-md-offset-2">

                        @if(!empty($review->comment))
                            @foreach($review->comment as $comment)
                            <div class="well">
                                <div class="author-info">
                                    <img src="" class="auth-image">

                                </div>

                                <div class="comment-content">
                                    {{ $comment->comment }}
                                </div>
                            </div>
                            @endforeach
                        @else
                            <h4>No comments are posted.</h4>
                        @endif
                        <hr>

                        @guest()
                            <small><a href="{{ route('login') }}">Login</a> to comment.</small>
                            @endguest
                            @auth
                                <div class="row">
                                    <form action="{{ route('comment.store') }}" method="post">
                                        <input type="text" name="comment" class="form-control">
                                        @csrf
                                        <input type="hidden" name="review_id" value="{{ $review->id }}">
                                        <input type="submit" value="Add Comment" class="btn btn-default">
                                    </form>
                                </div>
                            @endauth
                        @break($increment++ ==2)
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    </div>
    </div>


    <!-- //banner -->
    <!-- brands -->
    <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_popular">
        <div class="container">
            {{--<h5>{{ $product->name }} buyer also buys</h5>--}}
            <div class="w3ls_w3l_banner_nav_right_grid1">
                <h6>Accessories</h6>
                @foreach($product->subproduct as $product)
                    <div class="agile_top_brands_grids">

                        <div class="col-md-3 top_brand_left">
                            <div class="hover14 column">
                                <div class="agile_top_brand_left_grid">
                                    <div class="tag"><img src="{{ asset('images/tag.png') }}" alt=" " class="img-responsive" /></div>
                                    <div class="agile_top_brand_left_grid1">

                                        <figure>
                                            <div class="snipcart-item block" >
                                                <div class="snipcart-thumb">
                                                    <a href="{{ route('pages.single',$product->slug ) }}"><img title=" " alt=" " src="{{ url('images/'.$product->image)}}" class="img-responsive" /></a>
                                                    <p>{{ $product->name }}</p>
                                                    <h4>${{ $product->price }} <span>${{ $product->discount }}</span></h4>
                                                </div>

                                                <a href="{{ route('cart.accessories',$product->slug) }}" class="btn btn-primary">Add To Cart</a>
                                            </div>

                                        </figure>

                                    </div>
                                </div>
                            </div>

                        </div>
                        @endforeach

                <div class="clearfix"> </div>
            </div>
            <div class="w3ls_w3l_banner_nav_right_grid1">
                <h6>Best Deals</h6>
                <div class="col-md-3 w3ls_w3l_banner_left">
                    <div class="hover14 column">
                        <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                            <div class="agile_top_brand_left_grid_pos">
                                <img src="{{ asset('images/offer.png') }}" alt=" " class="img-responsive" />
                            </div>
                            <div class="agile_top_brand_left_grid1">
                                <figure>
                                    <div class="snipcart-item block">
                                        <div class="snipcart-thumb">
                                            <a href="single.html"><img src="images/9.png" alt=" " class="img-responsive" /></a>
                                            <p>fresh spinach (palak)</p>
                                            <h4>$2.00 <span>$3.00</span></h4>
                                        </div>
                                        <div class="snipcart-details">
                                            <form action="#" method="post">
                                                <fieldset>
                                                    <input type="hidden" name="cmd" value="_cart" />
                                                    <input type="hidden" name="add" value="1" />
                                                    <input type="hidden" name="business" value=" " />
                                                    <input type="hidden" name="item_name" value="fresh spinach" />
                                                    <input type="hidden" name="amount" value="2.00" />
                                                    <input type="hidden" name="discount_amount" value="1.00" />
                                                    <input type="hidden" name="currency_code" value="USD" />
                                                    <input type="hidden" name="return" value=" " />
                                                    <input type="hidden" name="cancel_return" value=" " />
                                                    <input type="submit" name="submit" value="Add to cart" class="button" />
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 w3ls_w3l_banner_left">
                    <div class="hover14 column">
                        <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                            <div class="agile_top_brand_left_grid_pos">
                                <img src="images/offer.png" alt=" " class="img-responsive" />
                            </div>
                            <div class="agile_top_brand_left_grid1">
                                <figure>
                                    <div class="snipcart-item block">
                                        <div class="snipcart-thumb">
                                            <a href="single.html"><img src="images/10.png" alt=" " class="img-responsive" /></a>
                                            <p>fresh mango dasheri (1 kg)</p>
                                            <h4>$5.00 <span>$8.00</span></h4>
                                        </div>
                                        <div class="snipcart-details">
                                            <form action="#" method="post">
                                                <fieldset>
                                                    <input type="hidden" name="cmd" value="_cart" />
                                                    <input type="hidden" name="add" value="1" />
                                                    <input type="hidden" name="business" value=" " />
                                                    <input type="hidden" name="item_name" value="fresh mango dasheri" />
                                                    <input type="hidden" name="amount" value="5.00" />
                                                    <input type="hidden" name="discount_amount" value="1.00" />
                                                    <input type="hidden" name="currency_code" value="USD" />
                                                    <input type="hidden" name="return" value=" " />
                                                    <input type="hidden" name="cancel_return" value=" " />
                                                    <input type="submit" name="submit" value="Add to cart" class="button" />
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 w3ls_w3l_banner_left">
                    <div class="hover14 column">
                        <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                            <div class="tag"><img src="images/tag.png" alt=" " class="img-responsive" /></div>
                            <div class="agile_top_brand_left_grid1">
                                <figure>
                                    <div class="snipcart-item block">
                                        <div class="snipcart-thumb">
                                            <a href="single.html"><img src="images/11.png" alt=" " class="img-responsive" /></a>
                                            <p>fresh apple red (1 kg)</p>
                                            <h4>$6.00 <span>$8.00</span></h4>
                                        </div>
                                        <div class="snipcart-details">
                                            <form action="#" method="post">
                                                <fieldset>
                                                    <input type="hidden" name="cmd" value="_cart" />
                                                    <input type="hidden" name="add" value="1" />
                                                    <input type="hidden" name="business" value=" " />
                                                    <input type="hidden" name="item_name" value="fresh apple red" />
                                                    <input type="hidden" name="amount" value="6.00" />
                                                    <input type="hidden" name="discount_amount" value="1.00" />
                                                    <input type="hidden" name="currency_code" value="USD" />
                                                    <input type="hidden" name="return" value=" " />
                                                    <input type="hidden" name="cancel_return" value=" " />
                                                    <input type="submit" name="submit" value="Add to cart" class="button" />
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 w3ls_w3l_banner_left">
                    <div class="hover14 column">
                        <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                            <div class="agile_top_brand_left_grid_pos">
                                <img src="images/offer.png" alt=" " class="img-responsive" />
                            </div>
                            <div class="agile_top_brand_left_grid1">
                                <figure>
                                    <div class="snipcart-item block">
                                        <div class="snipcart-thumb">
                                            <a href="single.html"><img src="images/12.png" alt=" " class="img-responsive" /></a>
                                            <p>fresh broccoli (500 gm)</p>
                                            <h4>$4.00 <span>$6.00</span></h4>
                                        </div>
                                        <div class="snipcart-details">
                                            <form action="#" method="post">
                                                <fieldset>
                                                    <input type="hidden" name="cmd" value="_cart" />
                                                    <input type="hidden" name="add" value="1" />
                                                    <input type="hidden" name="business" value=" " />
                                                    <input type="hidden" name="item_name" value="fresh broccoli" />
                                                    <input type="hidden" name="amount" value="4.00" />
                                                    <input type="hidden" name="discount_amount" value="1.00" />
                                                    <input type="hidden" name="currency_code" value="USD" />
                                                    <input type="hidden" name="return" value=" " />
                                                    <input type="hidden" name="cancel_return" value=" " />
                                                    <input type="submit" name="submit" value="Add to cart" class="button" />
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //brands -->
    <!-- newsletter -->
    <div class="newsletter">
        <div class="container">
            <div class="w3agile_newsletter_left">
                <h3>sign up for our newsletter</h3>
            </div>
            <div class="w3agile_newsletter_right">
                <form action="#" method="post">
                    <input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <input type="submit" value="subscribe now">
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //newsletter -->
@endsection