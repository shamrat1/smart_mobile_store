@extends('main')
@section('title','Your Companion')

@section('content')
{{--<div class="banner_bottom">--}}
    {{--<div class="wthree_banner_bottom_left_grid_sub">--}}
    {{--</div>--}}
    {{--<div class="wthree_banner_bottom_left_grid_sub1">--}}
        {{--<div class="col-md-4 wthree_banner_bottom_left">--}}
            {{--<div class="wthree_banner_bottom_left_grid">--}}
                {{--<img src="{{ asset('images/4.jpg') }}" alt=" " class="img-responsive" />--}}
                {{--<div class="wthree_banner_bottom_left_grid_pos">--}}
                    {{--<h4>Discount Offer <span>25%</span></h4>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-4 wthree_banner_bottom_left">--}}
            {{--<div class="wthree_banner_bottom_left_grid">--}}
                {{--<img src="{{ asset('images/5.jpg') }}" alt=" " class="img-responsive" />--}}
                {{--<div class="wthree_banner_btm_pos">--}}
                    {{--<h3>introducing <span>best store</span> for <i>Mobiles and Accessories</i></h3>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-4 wthree_banner_bottom_left">--}}
            {{--<div class="wthree_banner_bottom_left_grid">--}}
                {{--<img src="{{ asset('images/6.jpg') }}" alt=" " class="img-responsive" />--}}
                {{--<div class="wthree_banner_btm_pos1">--}}
                    {{--<h3>Save <span>Upto</span> $10</h3>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="clearfix"> </div>--}}
    {{--</div>--}}
    {{--<div class="clearfix"> </div>--}}
{{--</div>--}}
<!-- top-brands -->


<div class="top-brands">
<div class="container">
    <h3>Hot Offers</h3>
    @foreach($products as $product)
    <div class="agile_top_brands_grids">

        <div class="col-md-3 top_brand_left">
            <div class="hover14 column">
                <div class="agile_top_brand_left_grid">
                    <div class="tag"><img src="images/tag.png" alt=" " class="img-responsive" /></div>
                    <div class="agile_top_brand_left_grid1">

                        <figure>
                            <div class="snipcart-item block" >
                                <div class="snipcart-thumb">
                                    <a href="{{ route('pages.single',$product->slug ) }}"><img title=" " alt=" " src="{{ url('images/'.$product->images)}}" class="img-responsive" /></a>
                                    <p>{{ $product->name }}</p>
                                    <h4>${{ $product->unit_price }} <span>${{ $product->discount }}</span></h4>
                                </div>
                                <a href="{{ route('cart.create',$product->slug) }}"  class="btn btn-primary" data-toggle="#subproduct">Add To Cart</a>
                            </div>

                        </figure>
                        <div id="welcomeDiv"  style="display:none;" class="answer_list" >Working</div>
                    </div>
                </div>
            </div>

        </div>

        {{--//Modal--}}

        <div id="subproduct" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>



        @endforeach
        <div class="text-center">
        {{ $products->links() }}
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
</div>
{{--{{ dd($subprod) }}--}}
<!-- //top-brands -->
<!-- fresh-vegetables -->
@if(!empty($subprod->items))
<div class="fresh-vegetables">
    <div class="container">
        <h3>Recommanded for You</h3>
        @foreach($subprod as $product)
            <div class="agile_top_brands_grids">

                <div class="col-md-3 top_brand_left">
                    <div class="hover14 column">
                        <div class="agile_top_brand_left_grid">
                            <div class="tag"><img src="" alt=" " class="img-responsive" /></div>
                            <div class="agile_top_brand_left_grid1">

                                <figure>
                                    <div class="snipcart-item block" >
                                        <div class="snipcart-thumb">
                                            <a href="{{ route('accessories.single',$product->slug ) }}"><img title=" " alt=" " src="{{ url('images/'.$product->image)}}" class="img-responsive" /></a>
                                            <p>{{ $product->name }}</p>
                                            <h4>${{ $product->price }} <span>${{ $product->discount }}</span></h4>
                                        </div>
                                        <a href="{{ route('cart.accessories',$product->slug) }}" class="btn btn-primary">Add To Cart</a>
                                    </div>

                                </figure>

                            </div>
                        </div>
                    </div>

                </div>
                @endforeach
                <div class="text-center pagination">
                {{ $subprod->links() }}
                </div>
                <br>
                <br>
                <div class="clearfix"> </div>
            </div>
    </div>
</div>
@endif
<!-- //fresh-vegetables -->
<!-- fresh-vegetables -->
    <div class="fresh-vegetables">
        <div class="container">
            <h3>Best Deals</h3>
            @foreach($products as $product)
                @if($product->best_deal == 1)
                <div class="agile_top_brands_grids">

                    <div class="col-md-3 top_brand_left">
                        <div class="hover14 column">
                            <div class="agile_top_brand_left_grid">
                                <div class="tag"><img src="" alt=" " class="img-responsive" /></div>
                                <div class="agile_top_brand_left_grid1">

                                    <figure>
                                        <div class="snipcart-item block" >
                                            <div class="snipcart-thumb">
                                                <a href="{{ route('pages.single',$product->slug ) }}"><img title=" " alt=" " src="{{ url('images/'.$product->images)}}" class="img-responsive" /></a>
                                                <p>{{ $product->name }}</p>
                                                <h4>${{ $product->unit_price }} <span>${{ $product->discount }}</span></h4>
                                            </div>
                                            <a href="{{ route('cart.accessories',$product->slug) }}" class="btn btn-primary" onclick="showRacBox()">Add To Cart</a>
                                        </div>

                                    </figure>

                                </div>
                            </div>
                        </div>

                    </div>
                    @endif
                    @endforeach
                    {{--<div class="text-center">--}}
                        {{--{{ $products->links() }}--}}
                    {{--</div>--}}
                    <br>
                    <br>
                    <div class="clearfix"> </div>
                </div>

        </div>
    </div>

<div class="clearfix"> </div>

</div>
</div>
</div>
</div>
<!-- //fresh-vegetables -->
<!-- newsletter -->
<div class="newsletter">
<div class="container">
    <div class="w3agile_newsletter_left">
        <h3>sign up for our newsletter</h3>
    </div>
    <div class="w3agile_newsletter_right">
        <form action="#" method="post">
            <input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
            <input type="submit" value="subscribe now">
        </form>
    </div>
    <div class="clearfix"> </div>
</div>
</div>
<!-- //newsletter -->
<script type="text/javascript">

    $(document).ready(function() {
        function addToCart(value) {
            $.ajax({
                type: "GET",
                dataType: "json",
                url: "cart/ajaxCreate/"+value,
                success: function(data){
                    console.log(data);
                }
            });
        }


    });
</script>
@endsection