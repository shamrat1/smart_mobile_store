@extends('layouts.admin')
@section('title','Items on orders')
@section('content')

    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Quantity</th>
                <th scope="col">Product ID</th>
                <th scope="col">Action</th>
            </tr>
            </thead>

            <tbody>
            @foreach($items as $items)
                <tr>
                    <th scope="row">{{ $items->id }}</th>
                    <td>{{ $items->product_name }}</td>
                    <td>{{ $items->quantity }}</td>
                    <td><a href="{{ route('product.show',$items->product_id) }}">{{ $items->product_id }}</a></td>
                    <td>
                        <div class="row">
                            {{--@if($items->isApproved == 0)--}}
                                {{--{{ Form::open(['route'=>["items.isApproved",$items->id],'method'=>'PATCH']) }}--}}
                                {{--{{ Form::submit('Approve',['class'=>'btn btn-primary']) }}--}}
                                {{--{{ Form::close() }}--}}
                            {{--@endif--}}
                            {{--{{ Form::open(['route'=>["items.destroy",$items->id],'method'=>'DELETE']) }}--}}
                            {{--{{ Form::submit('Delete',['class'=>'btn btn-danger']) }}--}}
                            {{--{{ Form::close() }}--}}
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    @endsection