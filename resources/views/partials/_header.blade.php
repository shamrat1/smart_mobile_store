
    <!-- header -->
        <div class="agileits_header">
            <div class="w3l_offers">
                <a href="{{ url('/') }}">Today's special Offers !</a>
            </div>
            <div class="w3l_search">
                <form action="/search" method="get">
                    <input type="text" name="query" value="Search a product..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search a product...';}" required="">
                    <input type="submit" value=" ">
                </form>
            </div>
            <div class="product_list_header" style="margin-top: 5px">
                @if(Cart::count() >= 1)
                <button class="btn btn-success" style="margin-top: 3px;" data-toggle="modal" data-target="#cart">View Your Cart
                    <span>({{ Cart::count() }})</span></button>
                    @else
                    <button class="btn btn-default" disabled style="margin-top: 3px;">Cart ( empty )</button>
                    @endif
            </div>

            <div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Items in your cart.</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Subtotal</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(Cart::content() as $item)
                                    <tr>
                                        @if($item->qty >= 1)
                                            <th>{{ $item->name }}</th>
                                            <td>{{ $item->qty }}</td>
                                            <td>{{ $item->price }}</td>
                                            <td>{{ $item->total }}</td>
                                            <td><a href="{{ route('cart.increase',[$item->rowId,$item->qty]) }}" class="btn"><span class="glyphicon glyphicon-plus"></span></a>
                                                <a href="{{ route('cart.decrease',[$item->rowId,$item->qty]) }}" class="btn"><span class="glyphicon glyphicon-minus"></span></a>
                                                <a href="{{ route('cart.remove',$item->rowId) }}" class="btn"><span class="glyphicon glyphicon-trash"></span></a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a href="{{ route('cart.index') }}" type="button" class="btn btn-primary">Place an order</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w3l_header_right" style="padding-left: 8em !important;">
                <ul>
                    <li class="dropdown profile_details_drop">
                        @guest
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i><span class="caret"></span></a>
                        <div class="mega-dropdown-menu">
                            <div class="w3ls_vegetables">
                                <ul class="dropdown-menu drp-mnu">
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Sign Up</a></li>
                                </ul>
                                    @else
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name  }}&nbsp;<i class="fa fa-user" aria-hidden="true"></i>
                                        {{--<span class="caret"></span>--}}
                                    </a>
                                    <div class="mega-dropdown-menu">
                                        <div class="w3ls_vegetables">
                                            <ul class="dropdown-menu drp-mnu">
                                                <li><a href="{{ route('profile') }}">Profile</a></li>
                                                <li><a href="">Messages</a></li>
                                                <li><a class="dropdown-item" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form></li>
                                            </ul>
                                @endguest

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="w3l_header_right1">
                <h2><a href="{{ route('contact') }}">Contact Us</a></h2>
            </div>
            <div class="clearfix"> </div>
        </div>
    <!-- script-for sticky-nav -->
        <script>
        $(document).ready(function() {
             var navoffeset=$(".agileits_header").offset().top;
             $(window).scroll(function(){
                var scrollpos=$(window).scrollTop(); 
                if(scrollpos >=navoffeset){
                    $(".agileits_header").addClass("fixed");
                }else{
                    $(".agileits_header").removeClass("fixed");
                }
             });
             
        });
        </script>
    <!-- //script-for sticky-nav -->
        <div class="logo_products">
            <div class="container">
                <div class="w3ls_logo_products_left">
                    <h1><a href="{{ route('index') }}"><span>SM</span> Store</a></h1>
                </div>
                <div class="w3ls_logo_products_left1">
                    <ul class="special_items">
                        <li><a href="{{ route('events') }}">Events</a><i>/</i></li>
                        <li><a href="{{ route('about') }}">About Us</a><i>/</i></li>
                        <li><a href="{{ route('deals') }}">Best Deals</a><i>/</i></li>
                        <li><a href="{{ route('services') }}">Services</a></li>
                    </ul>
                </div>
                <div class="w3ls_logo_products_left1">
                    <ul class="phone_email">
                        <li><i class="fa fa-phone" aria-hidden="true"></i>(+0123) 234 567</li>
                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:store@grocery.com">store@grocery.com</a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    <!-- //header -->
    