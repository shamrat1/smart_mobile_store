@extends('layouts.admin')
@section('title','All Orders')
@section('breadcrumb','All Orders')

@section('content')

    <span class="glyphicon glyphicon-ok"></span>
    <div class="row">
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Mobile</th>
                <th scope="col">Address</th>
                <th scope="col">Payment Type</th>
                <th scope="col">Ordered on</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <th scope="row">{{ $order->id }}</th>
                    <td>{{ $order->name }}</td>
                    <td>{{ $order->mobile }}</td>
                    <td>{{ $order->address }}</td>
                    <td>{{ $order->payment_type }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>
                        <div class="row">
                            @if($order->approved == 0)
                                <a href="{{ route('order.approve',$order->id) }}" class="btn btn-success" style="padding-right: 10px">Approve</a>
                            @endif
                            {{ Form::open(['route'=>["orders.destroy",$order->id],'method'=>'DELETE']) }}
                            {{ Form::submit('Delete',['class'=>'btn btn-danger']) }}
                            {{ Form::close() }}



                        </div>
                    </td>
                </tr>

                @endforeach
            </tbody>

        </table>
    </div>


@endsection