@extends('alt')
@section('css')
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <style>
        .StripeElement {
            box-sizing: border-box;

            height: 40px;

            padding: 10px 12px;

            border: 1px solid #ccc;
            border-radius: 4px;
            background-color: white;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>
@endsection
@section('title','Your Shopping Cart')
@section('content')
<div class="w3l_banner_nav_right">
    <div class="col-xs-auto">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Price</th>
                    <th scope="col">Subtotal</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach(Cart::content() as $item)
                    <tr>
                        @if($item->qty >= 1)
                        <th>{{ $item->name }}</th>
                        <td>{{ $item->qty }}</td>
                        <td>{{ $item->price }}</td>
                        <td>{{ $item->total }}</td>
                        <td><a href="{{ route('cart.increase',[$item->rowId,$item->qty]) }}" class="btn"><span class="glyphicon glyphicon-plus"></span></a>
                            <a href="{{ route('cart.decrease',[$item->rowId,$item->qty]) }}" class="btn"><span class="glyphicon glyphicon-minus"></span></a>
                            <a href="{{ route('cart.remove',$item->rowId) }}" class="btn"><span class="glyphicon glyphicon-trash"></span></a>
                            </td>
                            @endif
                    </tr>
                @endforeach
                </tbody>

                <tfoot>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td>Subtotal</td>
                    <td><?php echo Cart::subtotal(); ?></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td>Tax</td>
                    <td><?php echo Cart::tax(); ?></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td>Total</td>
                    <td><?php echo Cart::total(); ?></td>
                </tr>
                </tfoot>
            </table>
        <hr>
    </div>
    <div class="row">
    <div class="col-md-6">
        <h4>Shipping Details</h4>
        {!! Form::open(['route'=>'order.store','data-parsley-validator','id'=>'payment-form']) !!}
        <section class="creditly-wrapper wthree, w3_agileits_wrapper">
                <div class="information-wrapper">
                    <div class="first-row form-group">
                        @php
                            if(Auth::user()){
                                $user = Auth::user()->name;}
                            else{
                                $user = null;
                            }

                        @endphp
                        <div class="controls">
                            {{ Form::label('name','Full Name:') }}
                            {{ Form::text('name',$user,['class'=>'form-control','required'=>'','minlength'=>'10']) }}
                        </div>
                        <div class="w3_agileits_card_number_grids">
                            <div class="w3_agileits_card_number_grid_left">
                                <div class="controls">
                                    {{ Form::label('Mobile','Mobile No:') }}
                                    {{--{{ Form::number('mobile',null,['class'=>'form-control']) }}--}}
                                    <input type="text" class="form-control" name="mobile">
                                </div>
                            </div>
                            <div class="w3_agileits_card_number_grid_right">
                                <div class="controls">
                                    {{ Form::label('address','Shipping Address:') }}
                                    {{ Form::text('address','Enter Shipping address',['class'=>'form-control','required'=>'','minlength'=>'4','maxlength'=>'190']) }}
                                </div>
                            </div>
                            <div class="clear"> </div>
                        </div>
                        <div class="controls">
                            {{ Form::label('city','Town/City:') }}
                            {{ Form::text('city',null,['class'=>'form-control','required'=>'']) }}
                        </div>
                        <div class="controls">
                                <div class="form-row">
                                    <label for="card-element">
                                        Credit or debit card
                                    </label>
                                    <div id="card-element">
                                        <!-- A Stripe Element will be inserted here. -->
                                    </div>

                                    <!-- Used to display form errors. -->
                                    <div id="card-errors" role="alert"></div>
                                </div>


                        </div>
                    </div>
                    {{Form::submit('Place Order', array('class'=>'btn btn-success'))}}
                </div>
            </section>
        {!! Form::close() !!}
    </div>
    </div>
    <hr>
</div>
    <div class="clearfix"></div>
@endsection
@section('extra-js')
    <script>
        // Create a Stripe client.
        var stripe = Stripe('{{ env("STRIPE_KEY") }}');

        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();

            var options = {
                name: document.getElementById('name').value,
                address_line1:document.getElementById('address').value,
                address_city: document.getElementById('city').value
            }

            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }
    </script>
    @endsection